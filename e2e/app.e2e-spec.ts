import { GoogleMapsDirectionPage } from './app.po';

describe('google-maps-direction App', () => {
  let page: GoogleMapsDirectionPage;

  beforeEach(() => {
    page = new GoogleMapsDirectionPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
