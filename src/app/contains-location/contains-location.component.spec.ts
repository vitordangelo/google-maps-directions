import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainsLocationComponent } from './contains-location.component';

describe('ContainsLocationComponent', () => {
  let component: ContainsLocationComponent;
  let fixture: ComponentFixture<ContainsLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainsLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainsLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
