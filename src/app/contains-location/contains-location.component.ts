import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core';

declare var google;

@Component({
  selector: 'app-contains-location',
  templateUrl: './contains-location.component.html',
  styleUrls: ['./contains-location.component.css']
})
export class ContainsLocationComponent implements OnInit {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  start = 'chicago, il';
  end = 'chicago, il';
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;

  latLng: any = {
    lat: -22.255413,
    lng: -45.693610
  };

  lat: number = -22.25331;
  lng: number = -45.700481;

  polygon: any = [
    { lat: -22.245807137742943, lng: -45.70870399475098 },
    { lat: -22.25331, lng: -45.700481 },
    { lat: -22.256293109879614, lng: -45.710248947143555 },
    { lat: -22.256746, lng: -45.694612 },
    { lat: -22.262166, lng: -45.70288 }
  ];


  constructor() {

  }

  ngOnInit() {
    this.initMap();

    google.maps.event.addListener(this.map, 'click', function(event) {
     var bermudaTriangle = new google.maps.Polygon({
      paths: [
        new google.maps.LatLng(25.774, -80.190),
        new google.maps.LatLng(18.466, -66.118),
        new google.maps.LatLng(32.321, -64.757)
      ]
    });
      console.log(google.maps.geometry.poly.containsLocation(event.latLng, bermudaTriangle));
    });
  }

  initMap() {
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 7,
      center: { lat: 41.85, lng: -87.65 }
    });

    this.directionsDisplay.setMap(this.map);
  }

  calculateAndDisplayRoute() {
    this.directionsService.route({
      origin: this.start,
      destination: this.end,
      travelMode: 'DRIVING'
    }, (response, status) => {
      if (status === 'OK') {
        this.directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

}


