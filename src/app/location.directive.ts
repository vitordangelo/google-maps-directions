import { Directive, Input } from '@angular/core';
import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core/services/google-maps-api-wrapper';

declare var google: any;

@Directive({
  selector: '[appLocation]'
})
export class LocationDirective {
  @Input() latLng;
  @Input() polygon;

  constructor(private gmapsApi: GoogleMapsAPIWrapper) { }

  ngOnInit() {}

}
