import { LocationsService } from './locations.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { DirectionsMapDirective } from './directions-map.directive';
import { ContainsLocationComponent } from './contains-location/contains-location.component';

import { AppRoutingModule } from './app.routing.module';
import { LocationDirective } from './location.directive';
import { ContainsLocationInatelComponent } from './contains-location-inatel/contains-location-inatel.component';

@NgModule({
  declarations: [
    AppComponent,
    DirectionsMapDirective,
    ContainsLocationComponent,
    LocationDirective,
    ContainsLocationInatelComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAoUy1JMgfJI9w4VXo7rFYcB6_XWvJpY3M'
    }),
    AppRoutingModule  
  ],
  providers: [
    LocationsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
