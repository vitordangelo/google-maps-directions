import { DirectionsMapDirective } from './directions-map.directive';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  origin = { longitude: -45.700162, latitude: -22.247912 };  
  destination = { longitude: -45.714841, latitude: -22.242797 }; 
}
