import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
declare var google;

@Component({
  selector: 'app-contains-location-inatel',
  templateUrl: './contains-location-inatel.component.html',
  styleUrls: ['./contains-location-inatel.component.css']
})
export class ContainsLocationInatelComponent implements OnInit {

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  constructor() { }

  ngOnInit() {
    this.initMap();

    google.maps.event.addListener(this.map, 'click', function (event) {
      var bermudaTriangle = new google.maps.Polygon({
        paths: [
          new google.maps.LatLng(-22.255508675122414 , -45.6952178478241),
          new google.maps.LatLng(-22.255995223780552, -45.694225430488586),
          new google.maps.LatLng(-22.25845276514273, -45.696473121643066),
          new google.maps.LatLng(-22.257400247659046, -45.69696664810181)
        ]
      });
      console.log(google.maps.geometry.poly.containsLocation(event.latLng, bermudaTriangle));
    });
  }

  initMap() {
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 18,
      center: { lat: -22.257089, lng: -45.695455 }
    });
  }

}