import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainsLocationInatelComponent } from './contains-location-inatel.component';

describe('ContainsLocationInatelComponent', () => {
  let component: ContainsLocationInatelComponent;
  let fixture: ComponentFixture<ContainsLocationInatelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainsLocationInatelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainsLocationInatelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
