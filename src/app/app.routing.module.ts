import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContainsLocationComponent } from './contains-location/contains-location.component';
import { ContainsLocationInatelComponent } from './contains-location-inatel/contains-location-inatel.component';

const appRoutes: Routes = [
    { path: '', component: ContainsLocationInatelComponent },
 ];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}